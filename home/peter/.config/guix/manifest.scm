;; This "manifest" file can be passed to 'guix package -m' to reproduce
;; the content of your profile.  This is "symbolic": it only specifies
;; package names.  To reproduce the exact same profile, you also need to
;; capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(specifications->manifest
  (list "graphviz"
        "rust-crates-index"
        "rust-crates-io"
        "guile"
        "fcitx5-gtk4"
        "fcitx5-chinese-addons"
        "fcitx5-configtool"
        "fcitx5"
        "gnupg"
        "wget"
        "curl"
        "python"
        "gcc-toolchain@10"
        "clang@13"
        "rust@1.61"
        "rust-yaml-rust"
        "rust-analyzer"
        "rust-rustc-version"
        "rust-home"
        "llvm@13"
        "elixir"
        "neofetch"
        "git"
        "fish"
        "zsh"
        "vim"))
