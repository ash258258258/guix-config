;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(add-to-load-path (dirname (current-filename)))

(use-modules
 (gnu home)
 (gnu packages)
 (gnu services)
 (guix gexp)
 (guix utils)
 (gnu packages gtk)
 (gnu packages fcitx5)
 (gnu home services shells))

(define (generate-gtk-immodule-cache gtk gtk-version . extra-pkgs)
  (define major+minor (version-major+minor gtk-version))

  (define build
    (with-imported-modules '((guix build utils)
                             (guix build union)
                             (guix build profiles)
                             (guix search-paths)
                             (guix records))
      #~(begin
          (use-modules (guix build utils)
                       (guix build union)
                       (guix build profiles)
                       (ice-9 popen)
                       (srfi srfi-1)
                       (srfi srfi-26))

          (define (immodules-dir pkg)
            (format #f "~a/lib/gtk-~a/~a/immodules"
                    pkg #$major+minor #$gtk-version))

          (let* ((moddirs (filter file-exists?
                                  (map immodules-dir
                                       (list #$gtk #$@extra-pkgs))))
                 (modules (append-map (cut find-files <> "\\.so$")
                                      moddirs))
                 (query (format #f "~a/bin/gtk-query-immodules-~a"
                                #$gtk:bin #$major+minor))
                 (pipe (apply open-pipe* OPEN_READ query modules)))

            ;; Generate a new immodules cache file.
            (dynamic-wind
              (const #t)
              (lambda ()
                (call-with-output-file #$output
                  (lambda (out)
                    (while (not (eof-object? (peek-char pipe)))
                      (write-char (read-char pipe) out))))
                #t)
              (lambda ()
                (close-pipe pipe)))))))

  (computed-file (string-append "gtk-query-immodules-" major+minor) build))


(home-environment
 (packages
  '())
 (services
  (list (service
         home-bash-service-type
         (home-bash-configuration
          (aliases '())
          (bashrc
           (list (local-file "dotfiles/bashrc" "bashrc")))
          (bash-profile
           (list (local-file "dotfiles/bash-profile" "bash_profile")))
          (environment-variables
           `(("GTK_IM_MODULE" . "fcitx")
             ("QT_IM_MODULE" . "fcitx")
             ("XMODIFIERS" . "@im=fcitx")
             ("GUIX_GTK3_IM_MODULE_FILE" .
              ,(generate-gtk-immodule-cache
                gtk+
                "3.0.0"
                #~(begin #$fcitx5-gtk:gtk3))))))))))
