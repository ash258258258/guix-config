;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules desktop networking ssh xorg)

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Shanghai")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "Guix_vmware")
  (users (cons* (user-account
                  (name "peter")
                  (comment "Peter,Z")
                  (group "users")
                  (home-directory "/home/peter")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service gnome-desktop-service-type)
            (service openssh-service-type)
            (service tor-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services)
    (modify-services %desktop-services
                   (guix-service-type
                    config => (guix-configuration
                       (inherit config)
                       (substitude-urls '("https://mirror.sjtu.edu.cn/guix/"
                                  "https://ci.guix.gnu.org"))
		       (substitude-urls
			(append (list "https://substitutes.nonguix.org")
			  %default-substitute-urls))
		       (authorized-keys
			(append (list (local-file "./signing-key.pub"))
			 %default-authorized-guix-keys))))))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  (initrd-modules
    (append '("mptspi") %base-initrd-modules))
  (swap-devices
    (list (uuid "aa73974a-f1e9-4347-9b89-47dfa4468e61")))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "84492fc8-68c6-447a-986b-1041a52dee86"
                     'ext4))
             (type "ext4"))
           %base-file-systems)))
